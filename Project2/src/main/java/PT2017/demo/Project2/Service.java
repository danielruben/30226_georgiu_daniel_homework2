package PT2017.demo.Project2;


import javax.swing.JTextField;

import java.sql.*;
import java.math.*;
import java.awt.TextArea;

public class Service extends Thread
{
	private int nr_queues;
	private QueueClients queues[];
	static int ID =0;
	static final Time t_start=new Time(System.currentTimeMillis());
	private Time sim_interval;
	private Time min_arriving;
	private Time max_arriving;
	private Time min_service;
	private Time max_service;
	private TextArea logger;
	private JTextField textFieldAvgW;
	private JTextField textFieldAvgS;
	private JTextField textFieldAvgE;
	
	public Service(int nr_queues, QueueClients[] queues,String name, Time sim_interval, Time min_arriving,
			Time max_arriving, Time min_service, Time max_service,TextArea logger,JTextField textFieldAvgW,JTextField textFieldAvgS,JTextField textFieldAvgE) {
		setName( name );
		ID=0;
		this.nr_queues = nr_queues;
		this.queues = queues;
		for( int i=0; i<nr_queues; i++){
			this.queues[ i ] =queues[ i ] ;
		}
		this.sim_interval = sim_interval;
		this.min_arriving = min_arriving;
		this.max_arriving = max_arriving;
		this.min_service = min_service;
		this.max_service = max_service;
		this.logger=logger;
		this.textFieldAvgE=textFieldAvgE;
		this.textFieldAvgW=textFieldAvgW;
		this.textFieldAvgS=textFieldAvgS;
	}
	
	private int min_index (){
		int index = 0;
		try
		{
			long min = queues[0].size_queue();
			for( int i=1; i<nr_queues; i++){
				long lung = queues[ i ].size_queue();
				if ( lung < min ){
					min = lung;
					index = i;
				}
				else
					if(lung==min)
					{
						if(queues[i].size_queue()!=0 && queues[index].size_queue()!=0 && 
								queues[i].getClients().getLast().getFinish_time().getTime()<queues[index].getClients().getLast().getFinish_time().getTime())
						{
							index = i;
						}
					}
			}
		}
		catch( InterruptedException e ){
			System.out.println( e.toString());
		}
		return index;
	}

	public int sim_end()
	{
		int k=0;
		for(int i=0;i<nr_queues;i++)
		{
			k+=queues[i].getClients().size();
		}
		return k;
	}
	
	public Time random_time(Time t_min,Time t_max)
	{
		long min,max,t;
		min=t_min.getTime();
		max=t_max.getTime();
		t=(long)(Math.random()*(max-min+1)+min);
		return new Time(t);
	}
	
	public Client random_client(int id) throws InterruptedException
	{
		Time service_time=random_time(min_service,max_service);
		return new Client(id,new Time(System.currentTimeMillis()),service_time,new Time(System.currentTimeMillis()));
	}
	
	public void run(){
		try
		{
			long sim=sim_interval.getTime();
			while( sim>=0)
			{
				Client c =random_client(++ID);
				int m = min_index();
				logger.append("Client :" +Long.toString( ID )+" adaugat la coada "+Integer.toString(m)+"\n");
				queues[ m ].add_client( c );
				Time arriving_time=random_time(min_arriving,max_arriving);
				sleep( arriving_time.getTime());
				sim-=arriving_time.getTime();
			}
			while(sim_end()!=0)
				sleep(1000);
			textFieldAvgW.setText(Float.toString(avg_waiting_time()/1000));
			textFieldAvgS.setText(Float.toString(avg_service_time()/1000));
			textFieldAvgE.setText(Float.toString(avg_empty_time()/1000));
		}
		catch( InterruptedException e ){
			System.out.println( e.toString());
		}
	}
	
	public float avg_waiting_time()
	{
		float sum=0;
		for(int i=0;i<nr_queues;i++)
			sum+=queues[i].average_waiting_time();
		return sum/nr_queues;
	}
	
	public float avg_service_time()
	{
		float sum=0;
		for(int i=0;i<nr_queues;i++)
			sum+=queues[i].average_service_time();
		return sum/nr_queues;
	}
	
	public float avg_empty_time()
	{
		long sum=0;
		for(int i=0;i<nr_queues;i++)
			sum+=queues[i].getEmpty_queue_time().getTime();
		return (float)sum/nr_queues;
	}
	
	/*
	public static void main( String args[] ){
		int i;
		int nr = 3;
		QueueClients c[] = new QueueClients[ nr ];
		for( i=0; i<nr; i++){
			c[ i ] = new QueueClients("Coada "+Integer.toString( i ));
			c[ i ].start();
		}
		Service p = new Service( nr , c, "Producator",new Time(20000),new Time(1000),new Time(3000),new Time(5000),new Time(10000));
		p.start();
	}*/
	
}