package PT2017.demo.Project2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.TextArea;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.sql.Time;
import java.awt.event.ActionEvent;
import java.awt.event.TextListener;
import java.awt.event.TextEvent;
import java.awt.Panel;
import javax.swing.Box;
import javax.swing.JList;
import java.awt.TextField;

public class GUI {

	private JFrame frame;
	private JTextField textFieldSimInt;
	private JTextField textFieldNrQ;
	private JTextField textFieldStime_min;
	private JTextField textFieldStime_max;
	private JTextField textFieldAtime_min;
	private JTextField textFieldAtime_max;
	private JTextField textFieldAvgW;
	private JTextField textFieldAvgS;
	private JTextField textFieldAvgE;
	private JTextField[] textFieldC=new JTextField[8];

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1038, 427);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLogger = new JLabel("Logger");
		lblLogger.setBounds(660, 0, 117, 23);
		frame.getContentPane().add(lblLogger);
		
		JLabel lblSimulationInterval = new JLabel("Simulation duration(s)");
		lblSimulationInterval.setHorizontalAlignment(SwingConstants.CENTER);
		lblSimulationInterval.setBounds(10, 261, 100, 14);
		frame.getContentPane().add(lblSimulationInterval);
		
		textFieldSimInt = new JTextField();
		textFieldSimInt.setBounds(13, 278, 83, 20);
		frame.getContentPane().add(textFieldSimInt);
		textFieldSimInt.setColumns(10);
		
		textFieldNrQ = new JTextField();
		textFieldNrQ.setColumns(10);
		textFieldNrQ.setBounds(10, 220, 86, 20);
		frame.getContentPane().add(textFieldNrQ);
		
		JLabel lblNrOfQueues = new JLabel("Nr of queues");
		lblNrOfQueues.setHorizontalAlignment(SwingConstants.CENTER);
		lblNrOfQueues.setBounds(10, 201, 89, 14);
		frame.getContentPane().add(lblNrOfQueues);
		
		textFieldStime_min = new JTextField();
		textFieldStime_min.setColumns(10);
		textFieldStime_min.setBounds(10, 162, 38, 20);
		frame.getContentPane().add(textFieldStime_min);
		
		textFieldStime_max = new JTextField();
		textFieldStime_max.setColumns(10);
		textFieldStime_max.setBounds(61, 162, 38, 20);
		frame.getContentPane().add(textFieldStime_max);
		
		JLabel lblServiceTime = new JLabel("Service time(s)");
		lblServiceTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblServiceTime.setBounds(10, 130, 86, 14);
		frame.getContentPane().add(lblServiceTime);
		
		JLabel lblMin = new JLabel("min");
		lblMin.setHorizontalAlignment(SwingConstants.CENTER);
		lblMin.setBounds(10, 143, 41, 14);
		frame.getContentPane().add(lblMin);
		
		JLabel lblMax = new JLabel("max");
		lblMax.setHorizontalAlignment(SwingConstants.CENTER);
		lblMax.setBounds(58, 143, 41, 14);
		frame.getContentPane().add(lblMax);
		
		textFieldAtime_min = new JTextField();
		textFieldAtime_min.setColumns(10);
		textFieldAtime_min.setBounds(10, 99, 38, 20);
		frame.getContentPane().add(textFieldAtime_min);
		
		textFieldAtime_max = new JTextField();
		textFieldAtime_max.setColumns(10);
		textFieldAtime_max.setBounds(61, 99, 38, 20);
		frame.getContentPane().add(textFieldAtime_max);
		
		JLabel label = new JLabel("min");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(10, 80, 41, 14);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("max");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setBounds(58, 80, 41, 14);
		frame.getContentPane().add(label_1);
		
		JLabel lblArrTimes = new JLabel("Arriving time(s)");
		lblArrTimes.setHorizontalAlignment(SwingConstants.CENTER);
		lblArrTimes.setBounds(10, 67, 86, 14);
		frame.getContentPane().add(lblArrTimes);
		
		textFieldAvgW = new JTextField();
		textFieldAvgW.setEditable(false);
		textFieldAvgW.setBounds(166, 322, 86, 20);
		frame.getContentPane().add(textFieldAvgW);
		textFieldAvgW.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("avg waiting time(s)");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(135, 296, 151, 14);
		frame.getContentPane().add(lblNewLabel);
		
		textFieldAvgS = new JTextField();
		textFieldAvgS.setEditable(false);
		textFieldAvgS.setColumns(10);
		textFieldAvgS.setBounds(319, 322, 86, 20);
		frame.getContentPane().add(textFieldAvgS);
		
		JLabel lblAvgTime = new JLabel("avg  service time(s)");
		lblAvgTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblAvgTime.setBounds(296, 296, 140, 14);
		frame.getContentPane().add(lblAvgTime);
		
		textFieldAvgE = new JTextField();
		textFieldAvgE.setEditable(false);
		textFieldAvgE.setColumns(10);
		textFieldAvgE.setBounds(486, 322, 86, 20);
		frame.getContentPane().add(textFieldAvgE);
		
		JLabel lblEmptyQueueTime = new JLabel("avg empty queue time(s)");
		lblEmptyQueueTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmptyQueueTime.setBounds(446, 296, 169, 14);
		frame.getContentPane().add(lblEmptyQueueTime);
		
		final TextArea Logger = new TextArea();
		Logger.setEditable(false);
		Logger.setBounds(660, 25, 362, 353);
		frame.getContentPane().add(Logger);
		
		JLabel lblEvolutieCozi = new JLabel("Queues evolution");
		lblEvolutieCozi.setBounds(166, 25, 100, 14);
		frame.getContentPane().add(lblEvolutieCozi);
		
		textFieldC[0] = new JTextField();
		textFieldC[0].setVisible(false);
		textFieldC[0].setEditable(false);
		textFieldC[0].setBounds(166, 258, 460, 20);
		frame.getContentPane().add(textFieldC[0]);
		textFieldC[0].setColumns(10);
		
		textFieldC[1] = new JTextField();
		textFieldC[1].setVisible(false);
		textFieldC[1].setEditable(false);
		textFieldC[1].setColumns(10);
		textFieldC[1].setBounds(166, 227, 460, 20);
		frame.getContentPane().add(textFieldC[1]);
		
		textFieldC[2] = new JTextField();
		textFieldC[2].setVisible(false);
		textFieldC[2].setEditable(false);
		textFieldC[2].setColumns(10);
		textFieldC[2].setBounds(166, 198, 460, 20);
		frame.getContentPane().add(textFieldC[2]);
		
		textFieldC[3] = new JTextField();
		textFieldC[3].setVisible(false);
		textFieldC[3].setEditable(false);
		textFieldC[3].setColumns(10);
		textFieldC[3].setBounds(166, 167, 460, 20);
		frame.getContentPane().add(textFieldC[3]);
		
		textFieldC[4] = new JTextField();
		textFieldC[4].setVisible(false);
		textFieldC[4].setEditable(false);
		textFieldC[4].setColumns(10);
		textFieldC[4].setBounds(166, 140, 460, 20);
		frame.getContentPane().add(textFieldC[4]);
		
		textFieldC[5] = new JTextField();
		textFieldC[5].setVisible(false);
		textFieldC[5].setEditable(false);
		textFieldC[5].setColumns(10);
		textFieldC[5].setBounds(166, 109, 460, 20);
		frame.getContentPane().add(textFieldC[5]);
		
		textFieldC[6] = new JTextField();
		textFieldC[6].setVisible(false);
		textFieldC[6].setEditable(false);
		textFieldC[6].setColumns(10);
		textFieldC[6].setBounds(166, 77, 460, 20);
		frame.getContentPane().add(textFieldC[6]);
		
		textFieldC[7] = new JTextField();
		textFieldC[7].setVisible(false);
		textFieldC[7].setEditable(false);
		textFieldC[7].setColumns(10);
		textFieldC[7].setBounds(166, 50, 460, 20);
		frame.getContentPane().add(textFieldC[7]);
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Logger.setText(null);
				int nr_queues=Integer.parseInt(textFieldNrQ.getText());
				long sim_int=Long.parseLong(textFieldSimInt.getText())*1000;
				long ser_min=Long.parseLong(textFieldStime_min.getText())*1000;
				long ser_max=Long.parseLong(textFieldStime_max.getText())*1000;
				long arr_min=Long.parseLong(textFieldAtime_min.getText())*1000;
				long arr_max=Long.parseLong(textFieldAtime_max.getText())*1000;
				QueueClients c[] = new QueueClients[ nr_queues];
				for( int i=0; i<8; i++)
				     textFieldC[i].setVisible(false);
				
				for( int i=0; i<nr_queues; i++){
					c[ i ] = new QueueClients("Coada "+Integer.toString( i ),Logger,textFieldC[i]);
					textFieldC[i].setVisible(true);
					c[ i ].start();
				}
				Service p = new Service( nr_queues , c, "Producator",new Time(sim_int),new Time(arr_min),new Time(arr_max),new Time(ser_min),new Time(ser_max),
						Logger,textFieldAvgW,textFieldAvgS,textFieldAvgE);
				p.start();
			}
		});
		btnStart.setBounds(10, 321, 89, 23);
		frame.getContentPane().add(btnStart);
		
	}
}
