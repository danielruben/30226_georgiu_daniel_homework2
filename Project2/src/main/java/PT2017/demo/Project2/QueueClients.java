package PT2017.demo.Project2;
import java.util.*;

import javax.swing.JTextField;

import java.awt.TextArea;
import java.sql.*;

public class QueueClients extends Thread
{
	private LinkedList<Client> clients=new LinkedList<Client>();
	private List<Time> waiting_time=new ArrayList<Time>();
	private List<Time> service_time=new ArrayList<Time>();
	private Time empty_queue_time=new Time(0);
	private TextArea logger;
	private JTextField textc;
	
	public QueueClients(String name,TextArea logger,JTextField textc)
	{
		setName( name );
		this.logger=logger;
		this.textc=textc;
	}
	
	public LinkedList<Client> getClients() {
		return clients;
	}

	public Time getEmpty_queue_time() {
		return empty_queue_time;
	}

	public synchronized void add_client( Client c ) throws InterruptedException
	{
		clients.add(c);
		display();
		notifyAll();
	}
	
	public void run(){
		try{
			while( true ){
				Client c=remove_client();
				sleep(c.getService_time().getTime());
				waiting_time.add(new Time(c.getFinish_time().getTime()-c.getArrival_time().getTime()));
				service_time.add(c.getService_time());
				c=clients.remove();
				logger.append(c.toString()+" a fost deservit de coada "+getName()+"\n");
				display();
				long k=size_queue();
			}
		}
		catch( InterruptedException e ){
			System.out.println("Intrerupere");
			System.out.println( e.toString());
		}
	}
	
	public synchronized Client remove_client() throws InterruptedException
	{
		Time t1=new Time(System.currentTimeMillis());
		while(clients.size()==0)
			wait();
		Time t2=new Time(System.currentTimeMillis());
		empty_queue_time.setTime(empty_queue_time.getTime()+t2.getTime()-t1.getTime());
		Client c=clients.element();
		c.setFinish_time(new Time(System.currentTimeMillis()+c.getService_time().getTime()));
		notifyAll();
		return c;
	}
	
	public synchronized long size_queue() throws InterruptedException{
		notifyAll();
		long size = clients.size();
		return size;
	}
	
	public float average_waiting_time()
	{
		long sum=0;
		if(waiting_time.size()==0)
			return 0;
		for(Time t:waiting_time)
		{
			sum+=t.getTime();
		}
		return (float)sum/waiting_time.size();
	}
	
	public float average_service_time()
	{
		long sum=0;
		if(service_time.size()==0)
			return 0;
		for(Time t:service_time)
		{
			sum+=t.getTime();
		}
		return (float)sum/service_time.size();
	}
	
	private void display()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(this.getName()+": ");
		for(int c=0;c<clients.size();c++)
			sb.append("Client "+Long.toString(clients.get(c).getId())+" ");
		textc.setText(sb.toString());
	}
}
