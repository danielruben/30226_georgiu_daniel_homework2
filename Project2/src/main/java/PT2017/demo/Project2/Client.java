package PT2017.demo.Project2;
import java.sql.Time;

public class Client
{
	private int id;
	private Time arrival_time;
	private Time service_time;
	private Time finish_time;
	
	public Client(int id, Time arrival_time, Time service_time, Time finish_time)
	{
		this.id = id;
		this.arrival_time = arrival_time;
		this.service_time = service_time;
		this.finish_time = finish_time;
	}

	public int getId() {
		return id;
	}

	public Time getArrival_time() {
		return arrival_time;
	}

	public Time getService_time() {
		return service_time;
	}

	public Time getFinish_time() {
		return finish_time;
	}
	
	public void setFinish_time(Time finish_time) {
		this.finish_time = finish_time;
	}

	public String toString()
	{
		return ( "Client "+Long.toString(id)+" "+arrival_time.toString()+" "+finish_time.toString()+"service:"+service_time.getTime());
	}
}